
# Table of Contents

1.  [Learning Log Home page](#org491e4db)

Making web pages with Django consists of three stages:

-   URLs
-   Views
-   Templates


<a id="org491e4db"></a>

# Learning Log Home page

In *learning\_log/urls.py* include learning\_logs urls for home page

    from django.contrib import admin
    from django.urls import path, include
    
    urlpatters = [
        path('admin', admin.site.urls),
        path('', include('learning_logs.urls')),
    ]

In the **learning\_logs** folder create **urls.py** add the below code

    from django.urls import path
    from . import views
    
    app_name = 'learning_logs'
    urlpatterns = [
            path('', views.index, name='index'),
    ]

Next create view for the home page

    from django.shortcuts import render
    
    def index(request):
        return render(request, 'learning_logs/index.html')

At last create the template in *learning\_logs/templates/learning\_logs/index.html*

    <p> Learning Log</p>
    <p> Learning Log helps you keep track of your learning, for any topic you're learning about.</p>

