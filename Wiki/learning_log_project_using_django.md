
# Table of Contents

1.  [Django](#orgd0141d2)
2.  [Learning Log Project](#org48192ff)
3.  [Setting up Project](#org776f9ed)
    1.  [Creating a Project](#org7102d58)
    2.  [Creating Database](#org580c035)
    3.  [Viewing the Project](#org75440c3)



<a id="orgd0141d2"></a>

# Django

Django is a high-level Python web framework that enables rapid development of secure and maintained websites.


<a id="org48192ff"></a>

# Learning Log Project

Learning Log is a web app that allows user to log the topics they&rsquo;re interested in and to make journal entries as they learn about each topic.


<a id="org776f9ed"></a>

# Setting up Project


<a id="org7102d58"></a>

## Creating a Project

    django-admin startproject learning_log .


<a id="org580c035"></a>

## Creating Database

    python manage.py migrate


<a id="org75440c3"></a>

## Viewing the Project

    python manage.py runserver

---

Next: [Starting an App](./starting_an_app.md)

