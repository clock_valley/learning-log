
# Table of Contents

1.  [Create an App](#orgd184f93)
2.  [File structure is now](#org379641c)
3.  [Create models](#org309a73c)
4.  [Activating Models](#orgdf708a1)
5.  [Setting up superuser](#orge57056c)
6.  [Registering models with Admin site](#orgb7cd9cd)
7.  [Migrating the models](#org757a5be)



<a id="orgd184f93"></a>

# Create an App

    python manage.py startapp learning_logs


<a id="org379641c"></a>

# File structure is now

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">├──</td>
<td class="org-left">db.sqlite3</td>
</tr>


<tr>
<td class="org-left">├──</td>
<td class="org-left">learning_log</td>
</tr>


<tr>
<td class="org-left">├──</td>
<td class="org-left">learning_logs</td>
</tr>


<tr>
<td class="org-left">└──</td>
<td class="org-left">manage.py</td>
</tr>
</tbody>
</table>


<a id="org309a73c"></a>

# Create models

In *learning\_logs/models.py* create two models **Topic** and **Entry** along with there respective attributes

    from django.db import models
    
    class Topic(models.Model):
        text = models.CharField(max_length=200)
        date_added = models.DateTimeField(auto_now_add=True)
    
        def __str__(self):
            return self.text
    
    class Entry(models.Model):
        topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
        text = models.TextField()
        date_added = models.DateTimeField(auto_now_add=True)
    
        class Meta:
            verbose_name_plural = 'entries'
    
        def __str__(self):
            return f"{self.text[:50]}..."


<a id="orgdf708a1"></a>

# Activating Models

In *learning\_log/settings.py* add the **learning\_logs** app to the installed application list


<a id="orge57056c"></a>

# Setting up superuser

    python manage.py createsuperuser


<a id="orgb7cd9cd"></a>

# Registering models with Admin site

    from django.contrib import admin
    from .models import Topic
    
    admin.site.register(Topic)
    
    admin.site.register(Entry)


<a id="org757a5be"></a>

# Migrating the models

    python manage.py makemigrations learning_logs

    python manage.py migrate

---

Previous: [Learning Log Project using Django](./learning_log_project_using_django.md)

